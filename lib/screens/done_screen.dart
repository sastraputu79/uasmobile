import 'package:flutter/material.dart';
import 'package:todolistapp/models/todo.dart';
import 'package:todolistapp/services/todo_service.dart';
import 'home_screen.dart';

class DoneScreen extends StatefulWidget {
  @override
  _DoneScreenState createState() => _DoneScreenState();
}

class _DoneScreenState extends State<DoneScreen> {
  ToDoService _toDoService;
  var _toDo = ToDo();

  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();

  // ignore: deprecated_member_use
  List<ToDo> _doneList = List<ToDo>();

  @override
  initState() {
    super.initState();
    getAllToDos();
  }

  getAllToDos() async {
    _toDoService = ToDoService();
    // ignore: deprecated_member_use
    _doneList = List<ToDo>();

    var toDos = await _toDoService.readToDo();
    toDos.forEach((toDo) {
      setState(() {
        var model = ToDo();
        model.id = toDo['id'];
        model.name = toDo['name'];
        model.description = toDo['description'];
        model.date = toDo['date'];
        model.category = toDo['category'];
        model.isFinished = toDo['isFinished'];

        if (model.isFinished != 0) {
          _doneList.add(model);
        }
      });
    });
  }

  _deleteFormDialog(BuildContext context, toDoID) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (param) {
          return AlertDialog(
            shape: ContinuousRectangleBorder(
                borderRadius: BorderRadius.circular(28.0)),
            actions: <Widget>[
              // ignore: deprecated_member_use
              FlatButton(
                onPressed: () => Navigator.pop(context),
                child: Text('NO'),
                textColor: Colors.white,
                color: Colors.green,
                shape: ContinuousRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
              ),
              // ignore: deprecated_member_use
              FlatButton(
                onPressed: () async {
                  var result = await _toDoService.deleteToDo(toDoID);
                  if (result > 0) {
                    print(result);
                    getAllToDos();
                    Navigator.pop(context);
                    showSuccessSnackBar('Delete Success!');
                  } else {
                    showSuccessSnackBar('Failed! :(');
                  }
                },
                child: Text('Delete'),
                textColor: Colors.white,
                color: Colors.red,
                shape: ContinuousRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
              ),
            ],
            title: Text('Delete Item?'),
          );
        });
  }

  showSuccessSnackBar(message) {
    var _snackBar = SnackBar(
      content: Text(
        message,
        style: TextStyle(color: Colors.yellowAccent),
      ),
      action: SnackBarAction(
          label: 'Hide', textColor: Colors.black, onPressed: () {}),
      backgroundColor: Colors.black,
    );
    // ignore: deprecated_member_use
    _globalKey.currentState.showSnackBar(_snackBar);
  }

  @override
  Widget build(BuildContext context) {
    if (_doneList.length == 0) {
      return Scaffold(
        appBar: AppBar(
          // ignore: deprecated_member_use
          leading: FlatButton(
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => HomeScreen()));
            },
            child: Icon(
              Icons.home,
              color: Colors.white,
            ),
            color: Colors.brown,
          ),
          title: Text('Save Note'),
        ),
        body: Center(
          child: Text(
            'Incomplete Items',
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
          ),
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: Colors.grey[800],
        key: _globalKey,
        appBar: AppBar(
          // ignore: deprecated_member_use
          leading: FlatButton(
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => HomeScreen()));
            },
            child: Icon(
              Icons.home,
              color: Colors.white,
            ),
            color: Colors.brown,
          ),
          title: Text('Save Note'),
        ),
        body: ListView.builder(
            itemCount: _doneList.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.only(top: 8.0, left: 16.0, right: 16.0),
                child: Card(
                  child: ListTile(
                    leading: IconButton(
                      icon: Icon(Icons.undo),
                      color: Colors.green,
                      onPressed: () async {
                        _toDo.id = _doneList[index].id;
                        print(_toDo.id);
                        _toDo.name = _doneList[index].name;
                        _toDo.description = _doneList[index].description;
                        _toDo.date = _doneList[index].date;
                        _toDo.category = _doneList[index].category;
                        _toDo.isFinished = 0;
                        var result = await _toDoService.updateToDo(_toDo);
                        if (result > 0) {
                          print(result);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DoneScreen()));
                        }
                      },
                    ),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(_doneList[index].name ?? 'Not Name ')
                      ],
                    ),
                    subtitle: Text(
                      _doneList[index].category ?? 'Not Category',
                      style: TextStyle(
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                    trailing: IconButton(
                      icon: Icon(Icons.delete_sweep),
                      color: Colors.red,
                      onPressed: () async {
                        _deleteFormDialog(context, _doneList[index].id);
                      },
                    ),
                  ),
                ),
              );
            }),
      );
    }
  }
}
