import 'package:flutter/material.dart';
import 'package:todolistapp/splash_scereen.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.brown,
          accentColor: Colors.brown),
      home: SplashScreen(),
    );
  }
}
